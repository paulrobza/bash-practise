#!/bin/bash
echo "welcome"
firstline=$(head -n 1 source/changelog.md)
read -a splitfirstline <<< $firstline
version=${splitfirstline[1]}
echo "you are building version $version"
echo "continue "1" for yes and "0" to exit"
read versioncontinue
if [ $versioncontinue == 1 ]
then
  echo "ok"
  for file in $(ls source/)
  do
    if [ $file == "secretinfo.md" ]
    then
      echo "Not including $file"
    else
    echo "Copying $file"
    pwd
    cp source/$file build/.
    fi
    echo "list of files in build $version" 
    ls build
  done
else
echo "Please come back when you are ready"
fi
